//
//  NewsService.swift
//  NewsView
//
//  Created by Алексей Остапенко on 19/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

final class NewsService: NewsServiceProtocol {
    
    private let transport: TransportProtocol
    private let database: FileDataBase
    
    init(transport: TransportProtocol) {
        self.transport = transport
        self.database = FileDataBase(fileName: "News")
    }
    
    func news(for country: String, completionHandler: @escaping (ServiceResponse<[Article]>) -> Void) {
        guard let country = APIConstatns.Country(rawValue: country) else {
            completionHandler(.fail(nil))
            return
        }
        self.transport.request(with: APIConstatns.news(for: country), jsonCompletionHandler: { [weak self] json in
            guard let strongSelf = self else { return }
            if let json = json {
                let articles: [Article] = ObjectParser.parseObject(fromJson: json)
                try? strongSelf.database.persist(objects: articles)
                completionHandler(.success(articles))
            }
            else {
                completionHandler(.fail(nil))
            }
        })
    }
    
    func savedNews() -> [Article] {
        return database.read()
    }
}
