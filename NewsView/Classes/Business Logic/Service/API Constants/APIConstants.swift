//
//  APIConstants.swift
//  NewsView
//
//  Created by Алексей Остапенко on 19/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct APIConstatns {
    
    static private let base = "https://newsapi.org/v2"
    static private let appKey = "c6a259d43a504519b3631c3a98cb718d"
    
    private init() {}
    
    enum Country: String {
        case russia = "ru"
    }
    
    enum Category: String {
        case business = "business"
    }
    
    static func news(for country: Country, with category: Category = .business) -> URL {
        let fullStr = base + "/top-headlines?country=\(country.rawValue)&category=\(category.rawValue)&apiKey=\(appKey)"
        return URL(string: fullStr)!
    }
    
}
