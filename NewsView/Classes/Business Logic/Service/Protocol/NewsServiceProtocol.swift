//
//  NewsServiceProtocol.swift
//  NewsView
//
//  Created by Алексей Остапенко on 19/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

protocol NewsServiceProtocol {
    
    /// Получение списка новостей
    ///
    /// - Parameter country: выбранная страна
    /// - Parameter completionHandler: полученные новости или ошибка
    func news(for country: String, completionHandler: @escaping (ServiceResponse<[Article]>) -> Void)
    
    /// Получение списка сохраненных новостей
    ///
    func savedNews() -> [Article]
    
}
