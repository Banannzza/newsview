//
//  ObjectParser.swift
//  NewsView
//
//  Created by Алексей Остапенко on 19/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct ObjectParser {
    
    private init() {}
    
    static func parseObject<ObjectType: Decodable>(fromJson json: [String: Any]) -> [ObjectType] {
        var objects = [ObjectType]()
        
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []), let object = try? JSONDecoder().decode(ObjectType.self, from: jsonData) {
            return [object]
        }
        else {
            for key in json.keys {
                if let dictionary = json[key] as? [String: Any] {
                    objects.append(contentsOf: parseObject(fromJson: dictionary))
                }
                if let array = json[key] as? [[String: Any]] {
                    for dictionary in array {
                        objects.append(contentsOf: parseObject(fromJson: dictionary))
                    }
                }
            }
        }
        return objects
    }
    
}
