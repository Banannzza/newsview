//
//  WebTransport.swift
//  NewsView
//
//  Created by Алексей Остапенко on 19/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

final class WebTransport: TransportProtocol {
    
    private let urlSession = URLSession(configuration: .default)
    
    func request(with url: URL, dataCompletionHandler: @escaping (Data?) -> Void) {
        urlSession.dataTask(with: url) { [weak self] data, response, error in
            guard self != nil else { return }
            if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                dataCompletionHandler(data)
                return
            }
            dataCompletionHandler(nil)
            }.resume()
    }
    
    func request(with url: URL, jsonCompletionHandler: @escaping ([String : Any]?) -> Void) {
        urlSession.dataTask(with: url) { [weak self] data, response, error in
            guard self != nil else { return }
            if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                if let serverResponse = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves){
                    jsonCompletionHandler(["response" : serverResponse])
                    return
                }
            }
            jsonCompletionHandler(nil)
            }.resume()
    }
    
}
