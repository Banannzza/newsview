//
//  TransportProtocol.swift
//  NewsView
//
//  Created by Алексей Остапенко on 19/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

protocol TransportProtocol {
    
    /// Получение данных по URL
    ///
    /// - Parameter url: адрес
    /// - Parameter dataCompletionHandler: полученные данные или nil при ошибке
    func request(with url: URL, dataCompletionHandler: @escaping (Data?) -> Void)
    
    /// Получение данных по URL
    ///
    /// - Parameter url: адрес
    /// - Parameter jsonCompletionHandler: полученный json или nil при ошибке
    func request(with url: URL, jsonCompletionHandler: @escaping ([String : Any]?) -> Void)
    
}
