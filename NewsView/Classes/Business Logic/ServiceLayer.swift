//
//  ServiceLayer.swift
//  NewsView
//
//  Created by Алексей Остапенко on 19/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

final class ServiceLayer {
    
    static let shared = ServiceLayer()
    let newsService: NewsServiceProtocol
    
    private init() {
        let transport = WebTransport()
        newsService = NewsService(transport: transport)
    }
    
}
