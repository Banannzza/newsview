//
//  Routable.swift
//  NewsView
//
//  Created by Алексей Остапенко on 20/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

protocol Routable {
    var rootViewController: UIViewController { get set }
}

extension Routable {
    internal func show<T>(storyboard: String, identifier: String?, configure: ((T) -> Void)?) where T : UIViewController {
        let storyboard = UIStoryboard.init(name: storyboard, bundle: nil)
        
        guard let controller = (identifier != nil
            ? storyboard.instantiateViewController(withIdentifier: identifier!)
            : storyboard.instantiateInitialViewController()) as? T
            else { return assertionFailure("Не найден VC в \(storyboard).") }
        
        configure?(controller)
        
        rootViewController.show(controller, sender: rootViewController)
    }
}
