//
//  NewsListViewController.swift
//  NewsView
//
//  Created by Алексей Остапенко on 19/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class NewsListViewController: UIViewController {
    
    // MARK: - Outlet
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Property
    
    private let presentationModel: NewsListPresentationModel
    private lazy var router = NewsListRouter(rootViewController: self)
    
    // MARK: - Lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        presentationModel = NewsListPresentationModel()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        presentationModel.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        presentationModel = NewsListPresentationModel()
        super.init(coder: aDecoder)
        presentationModel.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        presentationModel.obtainNews()
    }
    
    // MARK: - Common
    
    private func configureUI() {
        self.navigationController?.navigationBar.topItem?.title = self.title
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 40
        tableView.tableFooterView = UIView()
    }
}

// MARK: - UITableViewDataSource

extension NewsListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentationModel.viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = String(describing: NewsListTableCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! NewsListTableCell
        let model = presentationModel.viewModel[indexPath.row]
        cell.configure(from: model)
        cell.selectionStyle = .none
        return cell
    }
}

// MARK: - UITableViewDelegate

extension NewsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = presentationModel.viewModel[indexPath.row]
        self.router.showFullDescription(for: model)
    }
}

// MARK: - UISearchBarDelegate

extension NewsListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presentationModel.obtainNews(for: searchText)
    }
}

// MARK: - NewsListPresentationModelDelegate

extension NewsListViewController: NewsListPresentationModelDelegate {
    func viewModelDidLoad() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    func errorThrowed(with message: String) {
        self.presentationModel.obtainSavedNews()
    }
}
