//
//  NewsListViewModel.swift
//  NewsView
//
//  Created by Алексей Остапенко on 19/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct NewsListViewModel {
    
    let imageURL: URL
    let title: String
    let description: String
    let dateString: String
    
    init(from article: Article) {
        imageURL = article.imageURL
        title = article.title
        description = article.description
        dateString = article.convertedDateString
    }
    
}
