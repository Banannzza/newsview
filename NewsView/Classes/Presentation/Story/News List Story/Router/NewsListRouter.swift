//
//  NewsListRouter.swift
//  NewsView
//
//  Created by Алексей Остапенко on 20/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

struct NewsListRouter: Routable {
    
    var rootViewController: UIViewController
    
    func showFullDescription(for article: NewsListViewModel) {
        let vcIdentifier = String(describing: NewsDescriptionViewController.self)
        self.show(storyboard: vcIdentifier, identifier: vcIdentifier) { vc in
            guard let descriptionVC = vc as? NewsDescriptionViewController else { return }
            descriptionVC.article = article
        }
    }
    
}
