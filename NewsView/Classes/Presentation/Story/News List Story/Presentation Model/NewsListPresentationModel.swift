//
//  NewsListPresentationModel.swift
//  NewsView
//
//  Created by Алексей Остапенко on 19/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

final class NewsListPresentationModel {
    
    // MARK: - Property
    
    private var unfiltredViewModel = [NewsListViewModel]()
    private var filtredViewModel: [NewsListViewModel]?
    private var timer: Timer?
    var viewModel: [NewsListViewModel] {
        if let filtredViewModel = filtredViewModel {
            return filtredViewModel
        }
        return unfiltredViewModel
    }
    weak var delegate: NewsListPresentationModelDelegate?
    
    // MARK: - Methods
    
    func obtainNews() {
        ServiceLayer.shared.newsService.news(for: "ru") { [weak self] response in
            guard let strongSelf = self else { return }
            switch response {
            case .fail(let error):
                strongSelf.delegate?.errorThrowed(with: error?.localizedDescription ?? "Ошибка при обработке запроса")
            case .success(let articles):
                strongSelf.unfiltredViewModel = articles.map { NewsListViewModel(from: $0) }
                strongSelf.delegate?.viewModelDidLoad()
            }
        }
    }
    
    func obtainSavedNews() {
        self.unfiltredViewModel = ServiceLayer.shared.newsService.savedNews().map { NewsListViewModel(from: $0) }
        self.delegate?.viewModelDidLoad()
    }
    
    @objc private func timerTick(_ searchText: String) {
        if searchText.count > 0 {
            self.filtredViewModel = self.unfiltredViewModel.filter { viewModel in
                return viewModel.title.lowercased().contains(searchText.lowercased())
            }
            self.delegate?.viewModelDidLoad()
        }
        else {
            self.filtredViewModel = nil
            self.obtainNews()
        }
    }
    
    func obtainNews(for searchText: String) {
        timer?.invalidate()
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.timerTick(searchText)
            }
        } else {
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(timerTick), userInfo: searchText, repeats: false)
        }
    }
    
}

// MARK: - NewsListPresentationModelDelegate

protocol NewsListPresentationModelDelegate: AnyObject {
    func viewModelDidLoad()
    func errorThrowed(with message: String)
}
