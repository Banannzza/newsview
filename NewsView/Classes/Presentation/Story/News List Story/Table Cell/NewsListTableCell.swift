//
//  NewsListTableCell.swift
//  NewsView
//
//  Created by Алексей Остапенко on 19/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class NewsListTableCell: UITableViewCell {
    
    // MARK: - Outlet
    
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsDateLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        newsImageView.image = nil
        newsTitleLabel.text = nil
        newsDateLabel.text = nil 
    }
    
    // MARK: - Common
    
    func configure(from viewModel: NewsListViewModel) {
        newsTitleLabel.text = viewModel.title
        newsDateLabel.text = viewModel.dateString
        newsDateLabel.text = viewModel.dateString
        let imageURL = viewModel.imageURL
        DispatchQueue.global(qos: .userInteractive).async {
            if let imageData = try? Data(contentsOf: imageURL) {
                guard imageURL == viewModel.imageURL else { return }
                DispatchQueue.main.async { [weak self] in
                    self?.newsImageView.image = UIImage(data: imageData)
                }
            }
        }
    }
}
