//
//  NewsDescriptionViewController.swift
//  NewsView
//
//  Created by Алексей Остапенко on 20/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class NewsDescriptionViewController: UIViewController {
    
    // MARK: - Outlet
    
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsDescriptionLabel: UILabel!
    
    // MARK: - Property
    
    var article: NewsListViewModel!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    // MARK: - Common
    
    private func configureUI() {
        newsTitleLabel.text = article.title
        newsDescriptionLabel.text = article.description
        let imageURL = article.imageURL
        DispatchQueue.global(qos: .userInteractive).async {
            if let imageData = try? Data(contentsOf: imageURL) {
                DispatchQueue.main.async { [weak self] in
                    self?.newsImageView.image = UIImage(data: imageData)
                }
            }
        }
    }
}
