//
//  FileDataBase.swift
//  SoglasieAccountRoom
//
//  Created by Алексей Остапенко on 04/02/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class FileDataBase {
    
    private let fileName: String
    
    init(fileName: String) {
        self.fileName = fileName
    }
    
    func persist<ObjectType: Codable>(objects: [ObjectType]) throws {
        var savedObjects: [ObjectType] = self.readSavedData()
        savedObjects.append(contentsOf: objects)
        try self.saveData(objects: savedObjects)
    }
    
    func persist<ObjectType: Codable>(object: ObjectType) throws {
        try self.persist(objects: [object])
    }
    
    func read<ObjectType: Decodable>() -> [ObjectType] {
        return self.readSavedData()
    }
    
    func erase<ObjectType: Codable & Equatable>(object: ObjectType) throws {
        var savedObjects: [ObjectType] = self.readSavedData()
        if let index = savedObjects.index(of: object) {
            savedObjects.remove(at: index)
            try self.persist(objects: savedObjects)
        }
    }
    
    func loadObjectFromBundleResource<ObjectType: Decodable>(withName name: String) -> [ObjectType] {
        if let path = Bundle.main.path(forResource: name, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [String: Any] {
                    return ObjectParser.parseObject(fromJson: json)
                }
            }
            catch {
                return []
            }
        }
        return []
    }
    
    private func saveData<ObjectType: Encodable>(objects: [ObjectType]) throws {
        let jsonData = try JSONEncoder().encode(objects)
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(self.fileName)
            do {
                try jsonData.write(to: fileURL, options: .atomic)
            }
        }
    }
    
    private func readSavedData<ObjectType: Decodable>() -> [ObjectType] {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(self.fileName)
            do {
                let savedData = try Data(contentsOf: fileURL)
                let savedObjects = try JSONDecoder().decode([ObjectType].self, from: savedData)
                return savedObjects
            }
            catch {
                return []
            }
        }
        return []
    }
}
