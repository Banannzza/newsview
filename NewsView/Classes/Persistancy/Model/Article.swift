//
//  Article.swift
//  NewsView
//
//  Created by Алексей Остапенко on 19/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct Article: Codable {
    
    let title: String
    let description: String
    let date: String
    let imageURL: URL
    let sourceURL: URL
    
    var convertedDateString: String {
        return date.replacingOccurrences(of: "T", with: " ").replacingOccurrences(of: "Z", with: "")
    }
    
    enum CodingKeys: String, CodingKey {
        case title
        case description
        case date = "publishedAt"
        case imageURL = "urlToImage"
        case sourceURL = "url"
    }
    
}
